import newhope


### First create Alice public key for transmitting ###
alicePublicKey = newhope.createAlicePublicKey()
print("Alice public key", alicePublicKey)

### Here you need to send Alice public key to Bob ... ###
# Here we don't need this, because everything is in the same script


### Bob need to reformat the received public key message from Alice ###
aliceMessage = newhope.reformatReceivedMessageAlice(alicePublicKey)
print("Alice message:", aliceMessage)

### Now Bob can create his public and the shared private key from alice message ###
bobPublicKey, sharedPrivateKeyBob = newhope.createBobPublicPrivateKey(aliceMessage)
print("Bob public key:", bobPublicKey)
print("Bobs private key:", sharedPrivateKeyBob)

### He sends his public key now to Alice ###
# Again we don't have to do this, because everything is in the same script


### Alice now has to reformat the received public key from bob ###
bobMessage = newhope.reformatReceivedMessageBob(bobPublicKey)
print("Bob message:", bobMessage)

### Alice can now also create the shared private key ###
sharedPrivateKeyAlice = newhope.createPrivateKeyFromBobsPublicKeyForAlice(bobMessage)
print("Alice private key:", sharedPrivateKeyAlice)


### Comparing the private key from Bob and the private key from Alice ###
print("To compare")
print("Bobs private key:", sharedPrivateKeyBob)
print("Alice private key:", sharedPrivateKeyAlice)
