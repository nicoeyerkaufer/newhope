import poly
import params
import os
import hashlib
import sys
import struct

if sys.version_info[0] < 3 or sys.version_info[1] < 6:
	from Crypto.Hash import SHAKE128

# global variables:
a_key = []
b_key = []
s_hat = []


# get_noise returns a random sampling from a normal distribution in the NTT domain.


def get_noise():
	# get_noise_start = time.clock() * 1000
	coefficients = poly.get_noise()
	coefficients = poly.poly_ntt(coefficients)
	# get_noise_end = time.clock() * 1000
	# print("Get Noise: ", get_noise_end - get_noise_start)
	return coefficients


# shareda is a server-side function that takes the (decoded) message received
# from the client as an argument. It generates the shared key a_key.


def shareda(received):
	# shareda_start = time.clock() * 1000
	global a_key, s_hat
	(c_coeffs, b_coeffs) = received
	v_coeffs = poly.pointwise(s_hat, b_coeffs)
	v_coeffs = poly.invntt(v_coeffs)
	a_key = poly.rec(v_coeffs, c_coeffs)
	# shareda_end = time.clock() * 1000
	# print("Shareda: ", shareda_end - shareda_start)
	return


# sharedb is a client-side function that takes the (decoded) message received
# from the server as an argument. It generates the shared key b_key and returns
# a message in the form of a tuple. This message should be encoded using JSON or
# another portable format and transmitted (over an open channel) to the server.


def sharedb(received):
	# sharedb_start = time.clock() * 1000
	global b_key
	(pka, seed) = received
	a_coeffs = gen_a(seed)
	s_coeffs = get_noise()
	e_coeffs = get_noise()
	b_coeffs = poly.pointwise(a_coeffs, s_coeffs)
	b_coeffs = poly.add(b_coeffs, e_coeffs)
	v_coeffs = poly.pointwise(pka, s_coeffs)
	v_coeffs = poly.invntt(v_coeffs)
	e_prime = poly.get_noise()
	v_coeffs = poly.add(v_coeffs, e_prime)
	c_coeffs = poly.helprec(v_coeffs)
	b_key = poly.rec(v_coeffs, c_coeffs)
	# sharedb_end = time.clock() * 1000
	# print("sharedb: ", sharedb_end - sharedb_start)
	return (c_coeffs, b_coeffs)


# keygen is a server-side function that generates the private key s_hat and
# returns a message in the form of a tuple. This message should be encoded using
# JSON or another portable format and transmitted (over an open channel) to the
# client.


def keygen(verbose=False):
	# keygen_start = time.clock() * 1000
	global s_hat
	# seed_time = time.clock() * 1000
	seed = os.urandom(params.NEWHOPE_SEEDBYTES)  # NEWHOPE_SEEDBYTES == 32
	# seed_time_ende = time.clock() * 1000
	# print("Seed Time: ", seed_time_ende - seed_time, " ms")
	a_coeffs = gen_a(seed)
	s_coeffs = get_noise()
	s_hat = s_coeffs
	e_coeffs = get_noise()
	r_coeffs = poly.pointwise(s_coeffs, a_coeffs)
	p_coeffs = poly.add(e_coeffs, r_coeffs)
	# keygen_end = time.clock() * 1000
	# print("keygen: ", keygen_end - keygen_start)
	return (p_coeffs, seed)


# gen_a returns a list of random coefficients.


def gen_a(seed):
	hashing_algorithm = None
	if sys.version_info[0] < 3 or sys.version_info[1] < 6:
		hashing_algorithm = SHAKE128.new()
	else:
		hashing_algorithm = hashlib.shake_128()
	hashing_algorithm.update(seed)
	# 2200 bytes from SHAKE-128 function is not always enough data to get 1024 coefficients --> 2688
	# smaller than 5q, from Alkim, Ducas, Pöppelmann, Schwabe section 7:
	shake_output = None
	shake_digest = 2688
	if sys.version_info[0] < 3 or sys.version_info[1] < 6:
		shake_output = hashing_algorithm.read(shake_digest)
	else:
		shake_output = hashing_algorithm.digest(shake_digest)
	output = []
	j = 0
	for i in range(0, params.N):
		coefficient = 5 * params.Q
		# Reject coefficients that are greater than or equal to 5q:
		while coefficient >= 5 * params.Q:
			coefficient = int.from_bytes(
				shake_output[j * 2: j * 2 + 2], byteorder='little')
			# print('j=' + str(j))
			j += 1
			if j * 2 >= len(shake_output):
				shake_digest += 100
				if sys.version_info[0] < 3 or sys.version_info[1] < 6:
					shake_output = hashing_algorithm.read(shake_digest)
				else:
					shake_output = hashing_algorithm.digest(shake_digest)
				# exit(1)
		output.append(coefficient)
		# print('chose ' + str(coefficient))
	# seed_end = time.clock() * 1000
	# print("seed: ", seed_end - seed_start)
	return output

	### Create Alice public key ###
def createAlicePublicKey():
	aliceMessage = keygen(True)
	aliceMessageTransportReady = formatForTransportAlice(aliceMessage)
	return aliceMessageTransportReady

	### Prepare Alice message for transport ###
def formatForTransportAlice(aliceMessage):
	transportAliceMessage = [0, 0]
	s1 = struct.Struct('<' + 1024 * 'L')
	transportAliceMessage[0] = s1.pack(*aliceMessage[0])
	s2 = struct.Struct('<' + 32 * 'H')
	transportAliceMessage[1] = s2.pack(*aliceMessage[1])
	return transportAliceMessage

	### Reformat Alice message for Bob ###
def reformatReceivedMessageAlice(aliceMessageTransportReady):
	aliceMessage = [0,0]
	s1 = struct.Struct('<' + 1024 * 'L')
	s2 = struct.Struct('<' + 32 * 'H')
	aliceMessage[0] = s1.unpack(aliceMessageTransportReady[0])
	aliceMessage[1] = bytes(s2.unpack(aliceMessageTransportReady[1]))
	return aliceMessage

### Create Key ### Create Bob message from alice key
def createBobPublicPrivateKey(aliceMessage):
	bobMessage = sharedb(aliceMessage)
	bobMessageTransportReady = formatForTransportBob(bobMessage)
	return bobMessageTransportReady, b_key

### Prepare Bob message for transport ###
def formatForTransportBob(bobMessage):
	transportBobMessage = [0,0]
	b1 = struct.Struct('<' + 1024 * 'b')
	transportBobMessage[0] = b1.pack(*bobMessage[0])
	b2 = struct.Struct('<' + 1024 * 'H')
	transportBobMessage[1] = b2.pack(*bobMessage[1])
	return transportBobMessage

	### Reformat Bob message for Alice ###
def reformatReceivedMessageBob(bobMessageTransportReady):
	bobMessage = [0,0]
	b1 = struct.Struct('<' + 1024 * 'b')
	b2 = struct.Struct('<' + 1024 * 'H')
	bobMessage[0] = b1.unpack(bobMessageTransportReady[0])
	bobMessage[1] = b2.unpack(bobMessageTransportReady[1])
	return bobMessage

def createPrivateKeyFromBobsPublicKeyForAlice(bobMessage):
	shareda(bobMessage)
	return a_key

