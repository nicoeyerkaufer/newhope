# NewHope for Python

This is a slightly modified version of the python implementation (Source: https://github.com/scottwn/PyNewHope) from a work that was submitted as a master's capstone advanced lab to the computer science department at the Courant Institute of Mathematical Sciences at New York University. It should be considered open source, free to use and modify.

NewHope is a quantum secure algorithm proposed by Alkim, Ducas, Pöppelmann, and Schwabe:  https://eprint.iacr.org/2015/1092

# How to start

Just clone the git repository and then go to `test_newhope.py`. There you will find the routine for an asymmetric post quantum key exchange.

# Changes made

The `shake_output` from `hashing_algorithm.digest()` was changed from 2200 to 2688. This change ensures that the probability of not enough data from the `hashing_algorithm.digest()` function is very low.
In addition, the functionality has been expanded. In the unlikely event that insufficient data is generated, additional values are retrieved from the `hashing_algorithm.digest()`.
Furthermore functions were added to format the exchanged messages. Both (b, seed) and (u, r) are encoded as an array of  4160 bytes or 3072 bytes.

It is now possible to run it on Python < 3.6 (you need to `pip install pycryptodome` for that).
